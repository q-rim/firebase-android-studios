package com.q_rim.q_firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;

    String TAG = "---QQQ: ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Firebase-Analytics:  Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Firebase-Analytics:
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "test-id");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "test-name");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "test-image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


        // Write a message to the database
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef1 = database.getReference("message");
        DatabaseReference dbRef2 = database.getReference("test");

        dbRef1.setValue("messageValue");
        dbRef2.setValue("nameValue");

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "Button Clicked");

                // Get String value from EditText
                EditText textInEditText = (EditText) findViewById(R.id.editText);
                final String textIn = textInEditText.getText().toString();

                Toast.makeText(MainActivity.this, "write to Firebase\nvalue: " + textIn, Toast.LENGTH_SHORT).show();

                // WRITE to Firebase
                DatabaseReference dbRef3 = database.getReference();
                DatabaseReference dbRefWrite = dbRef3.child("L1").child("L2").push();       // write to:  /L1/L2/<hash>
                dbRefWrite.setValue(textIn, new DatabaseReference.CompletionListener() {    // setValue() w/callback option
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Log.i(TAG, "onComplete()");
                        Toast.makeText(MainActivity.this, "onComplete() " +
                                "\nwriting to Firebase:  " + textIn +
                                "\ndatabaseError:  " + databaseError, Toast.LENGTH_SHORT).show();

                        TextView textView = (TextView) findViewById(R.id.textView);
                        textView.setText("databaseError: \n" + databaseError);

                        if (databaseError != null) {
                            // TODO: Do something as needed
                        }
                    }
                });
            }
        });

        // Read "myRef" from the database
        dbRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "myRef - onDataChange");
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.d(TAG, "myRef - onCancelled");
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        // Read "nameRef" from the database
        dbRef2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "nameRef - onDataChange");
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.d(TAG, "nameRef - onCancelled");
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }
}
